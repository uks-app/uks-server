package com.javainuse.model;


import javax.persistence.*;

@Entity
@Table(name = "tindakan")
public class TindakanModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String tindakan;



    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public String getTindakan() {
        return tindakan;
    }

    public void setTindakan(String tindakan) {
        this.tindakan = tindakan;
    }


}

package com.javainuse.model;


import javax.persistence.*;

@Entity
@Table(name = "penanganan")
public class PenangananModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String penanganan;




    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public String getPenanganan() {
        return penanganan;
    }

    public void setPenanganan(String penanganan) {
        this.penanganan = penanganan;
    }


}

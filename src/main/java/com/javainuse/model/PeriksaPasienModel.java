package com.javainuse.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

@Entity
@Table(name = "periksa_pasien")
public class PeriksaPasienModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String keterangan;
    @ManyToOne
    private SiswaModel siswa;
    @ManyToOne
    private GuruModel guru;
    @ManyToOne
    private KaryawanModel karyawan;
    @ManyToOne
    private StatusModel status;

    public PeriksaPasienModel(long id, String keterangan, SiswaModel siswa, GuruModel guru, KaryawanModel karyawan, StatusModel status) {
        this.id = id;
        this.keterangan = keterangan;
        this.siswa = siswa;
        this.guru = guru;
        this.karyawan = karyawan;
        this.status = status;
    }

    public PeriksaPasienModel() {

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public SiswaModel getSiswa() {
        return siswa;
    }

    public void setSiswa(SiswaModel siswa) {
        this.siswa = siswa;
    }

    public GuruModel getGuru() {
        return guru;
    }

    public void setGuru(GuruModel guru) {
        this.guru = guru;
    }

    public KaryawanModel getKaryawan() {
        return karyawan;
    }

    public void setKaryawan(KaryawanModel karyawan) {
        this.karyawan = karyawan;
    }

    public StatusModel getStatus() {
        return status;
    }

    public void setStatus(StatusModel status) {
        this.status = status;
    }
}

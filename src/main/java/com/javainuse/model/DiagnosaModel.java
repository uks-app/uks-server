package com.javainuse.model;


import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "diagnosa")
public class DiagnosaModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String diagnosa;




    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public String getDiagnosa() {
        return diagnosa;
    }

    public void setDiagnosa(String diagnosa) {
        this.diagnosa = diagnosa;
    }


}

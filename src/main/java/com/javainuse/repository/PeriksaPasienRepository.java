package com.javainuse.repository;

import com.javainuse.model.PeriksaPasienModel;
import com.javainuse.model.SiswaModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository

public interface PeriksaPasienRepository extends CrudRepository<PeriksaPasienModel, Integer> {
    PeriksaPasienModel findById(long id);

}

package com.javainuse.repository;

import com.javainuse.model.TindakanModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository

public interface TindakanRepository extends CrudRepository<TindakanModel, Integer> {
    TindakanModel findById(long id);

}

package com.javainuse.repository;

import com.javainuse.model.DiagnosaModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository

public interface DiagnosaRepository extends CrudRepository<DiagnosaModel, Integer> {
    DiagnosaModel findById(long id);

}

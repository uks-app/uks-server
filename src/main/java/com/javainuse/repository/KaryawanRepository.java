package com.javainuse.repository;

import com.javainuse.model.KaryawanModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository

public interface KaryawanRepository extends CrudRepository<KaryawanModel, Integer> {
    KaryawanModel findById(long id);

}

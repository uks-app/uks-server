package com.javainuse.repository;

import com.javainuse.model.GuruModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository

public interface GuruRepository extends CrudRepository<GuruModel, Integer> {
    GuruModel findById(long id);

}

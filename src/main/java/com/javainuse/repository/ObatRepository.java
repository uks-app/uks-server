package com.javainuse.repository;

import com.javainuse.model.ObatModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ObatRepository extends CrudRepository<ObatModel, Integer> {
    ObatModel findById(long id);

}

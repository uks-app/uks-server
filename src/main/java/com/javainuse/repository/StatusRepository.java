package com.javainuse.repository;

import com.javainuse.model.StatusModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository

public interface StatusRepository extends CrudRepository<StatusModel, Integer> {
    StatusModel findById(long id);

}

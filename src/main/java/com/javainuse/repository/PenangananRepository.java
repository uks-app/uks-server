package com.javainuse.repository;

import com.javainuse.model.PenangananModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository

public interface PenangananRepository extends CrudRepository<PenangananModel, Integer> {
    PenangananModel findById(long id);

}

package com.javainuse.repository;

import com.javainuse.model.SiswaModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository

public interface SiswaRepository extends CrudRepository<SiswaModel, Integer> {
   SiswaModel findById(long id);

}

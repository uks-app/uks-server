package com.javainuse.controller;

import com.javainuse.DTO.ObatDTO;
import com.javainuse.model.ObatModel;
import com.javainuse.service.JwtObatDetailsService;
import com.javainuse.util.CustomErrorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:3000")
public class ObatController {

    public static final Logger logger = LoggerFactory.getLogger(SiswaController.class);

    @Autowired
    private JwtObatDetailsService obatDetailsService;


    //--------------------- Create a Obat ---------------------------------

    @RequestMapping(value = "/obat/add", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> createObat(@RequestBody ObatDTO obat) throws SQLException, ClassNotFoundException {
        logger.info("Creating Obat : {}",obat);

        obatDetailsService.save(obat);

        return new ResponseEntity<>(obat, HttpStatus.CREATED);
    }

    // -------------------Retrieve All Obat--------------------------------------------

    @RequestMapping(value = "/obat", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<ObatModel>> listAllObat() throws SQLException, ClassNotFoundException {

        List<ObatModel> obats = obatDetailsService.findAll();

        return new ResponseEntity<>(obats, HttpStatus.OK);
    }

    // -------------------Retrieve Single Obat By Id------------------------------------------

    @RequestMapping(value = "/obat/id/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getObat(@PathVariable("id") long id) throws SQLException, ClassNotFoundException {
        logger.info("Fetching Obat with id {}", id);

        Optional<ObatModel> obat = obatDetailsService.findById(id);

        if (obat == null) {
            logger.error("Obat with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Obat with id " + id + " not found"), HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(obat, HttpStatus.OK);
    }

    // ------------------- Update Guru ------------------------------------------------
    @RequestMapping(value = "/obat/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateObat(@PathVariable("id") long id, @RequestBody ObatDTO obat) throws SQLException, ClassNotFoundException {
        logger.info("Updating obat with id {}", id);

        Optional<ObatModel> currentObat = obatDetailsService.findById(id);
        if (currentObat == null) {
            logger.error("Unable to update. Obat with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Unable to update. Obat with id " + id + " not found."), HttpStatus.NOT_FOUND);
        }
        currentObat.orElseThrow().setObat(obat.getObat());
        currentObat.orElseThrow().setStock(obat.getStock());
        currentObat.orElseThrow().setExpired(obat.getExpired());

        obatDetailsService.update(currentObat.get().getId());
        return new ResponseEntity<>(currentObat, HttpStatus.OK);

    }

    // ------------------- Delete Guru-----------------------------------------

    @RequestMapping(value = "/obat/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteobat(@PathVariable("id") long id) throws SQLException, ClassNotFoundException {
        logger.info("Fetching & Deleting Obat with id {}", id);

        obatDetailsService.delete(id);
        return new ResponseEntity<ObatModel>(HttpStatus.NO_CONTENT);
    }
}

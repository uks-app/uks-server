package com.javainuse.controller;

import com.javainuse.DTO.GuruDTO;
import com.javainuse.DTO.PeriksaPasienDTO;
import com.javainuse.model.GuruModel;
import com.javainuse.model.PeriksaPasienModel;
import com.javainuse.repository.PeriksaPasienRepository;
import com.javainuse.service.JwtGuruDetailsService;
import com.javainuse.service.JwtPeriksaPasienDetailsService;
import com.javainuse.util.CustomErrorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:3000")
public class DaftarPasienController {

    public static final Logger logger = LoggerFactory.getLogger(DaftarPasienController.class);

    @Autowired
    private JwtPeriksaPasienDetailsService periksaDetailsService;
    @Autowired
    PeriksaPasienRepository periksaPasienRepository;


    //--------------------- Create a Periksa Pasien ---------------------------------

    @RequestMapping(value = "/periksa/add", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> createPeriksa(@RequestBody PeriksaPasienDTO periksa) throws SQLException, ClassNotFoundException {
        logger.info("Creating Periksa Pasien : {}",periksa);

        periksaDetailsService.save(periksa);

        return new ResponseEntity<>(periksa, HttpStatus.CREATED);
    }

    // -------------------Retrieve All Periksa Pasien--------------------------------------------

    @RequestMapping(value = "/periksa", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<PeriksaPasienModel>> listAllPeriksa() throws SQLException, ClassNotFoundException {

        List<PeriksaPasienModel> periksas = periksaDetailsService.findAll();

        return new ResponseEntity<>(periksas, HttpStatus.OK);
    }

    // -------------------Retrieve Single Periksa Pasien By Id------------------------------------------

    @RequestMapping(value = "/periksa/id/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getPeriksa(@PathVariable("id") long id) throws SQLException, ClassNotFoundException {
        logger.info("Fetching Periksa Pasien with id {}", id);

        Optional<PeriksaPasienModel> periksa = periksaDetailsService.findById(id);

        if (periksa == null) {
            logger.error("Periksa with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Periksa with id " + id + " not found"), HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(periksa, HttpStatus.OK);
    }

    // ------------------- Update Periksa Pasien ------------------------------------------------
    @RequestMapping(value = "/periksa/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updatePeriksaPasien(@PathVariable("id") long id, @RequestBody PeriksaPasienDTO periksa) throws SQLException, ClassNotFoundException {
        logger.info("Updating Periksa with id {}", id);

        Optional<PeriksaPasienModel> currentPeriksaPasien = periksaDetailsService.findById(id);

        if (currentPeriksaPasien == null) {
            logger.error("Unable to update. Periksa Pasien with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Unable to update. Guru with id " + id + " not found."), HttpStatus.NOT_FOUND);
        }
        currentPeriksaPasien.orElseThrow().setKeterangan(periksa.getKeterangan());


        periksaDetailsService.update(currentPeriksaPasien.get().getId());
        return new ResponseEntity<>(currentPeriksaPasien, HttpStatus.OK);

    }

    // ------------------- Delete Guru-----------------------------------------

    @RequestMapping(value = "/periksa/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deletePeriksaPasien(@PathVariable("id") long id) throws SQLException, ClassNotFoundException {
        logger.info("Fetching & Deleting Periksa Pasien with id {}", id);

        periksaDetailsService.delete(id);
        return new ResponseEntity<PeriksaPasienModel>(HttpStatus.NO_CONTENT);
    }
}

package com.javainuse.controller;

import com.javainuse.DTO.SiswaDTO;
import com.javainuse.model.SiswaModel;
import com.javainuse.service.JwtSiswaDetailsService;
import com.javainuse.util.CustomErrorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:3000")
public class SiswaController {

    public static final Logger logger = LoggerFactory.getLogger(DaftarPasienController.class);

    @Autowired
    private JwtSiswaDetailsService siswaDetailsService;


    //--------------------- Create a Siswa ---------------------------------

    @RequestMapping(value = "/siswa/add", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> createSiswa(@RequestBody SiswaDTO siswa) throws SQLException, ClassNotFoundException {
        logger.info("Creating Siswa : {}",siswa);

        siswaDetailsService.save(siswa);

        return new ResponseEntity<>(siswa, HttpStatus.CREATED);
    }

    // -------------------Retrieve All Guru--------------------------------------------

    @RequestMapping(value = "/siswa", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<SiswaModel>> listAllSiswa() throws SQLException, ClassNotFoundException {

        List<SiswaModel> siswas = siswaDetailsService.findAll();

        return new ResponseEntity<>(siswas, HttpStatus.OK);
    }

    // -------------------Retrieve Single Guru By Id------------------------------------------

    @RequestMapping(value = "/siswa/id/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getSiswa(@PathVariable("id") long id) throws SQLException, ClassNotFoundException {
        logger.info("Fetching Siswa with id {}", id);

        Optional<SiswaModel> siswa = siswaDetailsService.findById(id);

        if (siswa == null) {
            logger.error("Siswa with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Siswa with id " + id + " not found"), HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(siswa, HttpStatus.OK);
    }

    // ------------------- Update Guru ------------------------------------------------
    @RequestMapping(value = "/siswa/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateSiswa(@PathVariable("id") long id, @RequestBody SiswaDTO siswa) throws SQLException, ClassNotFoundException {
        logger.info("Updating siswa with id {}", id);

        Optional<SiswaModel> currentSiswa = siswaDetailsService.findById(id);

        if (currentSiswa == null) {
            logger.error("Unable to update. Siswa with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Unable to update. Siswa with id " + id + " not found."), HttpStatus.NOT_FOUND);
        }
        currentSiswa.orElseThrow().setSiswa(siswa.getSiswa());
        currentSiswa.orElseThrow().setLahir(siswa.getLahir());
        currentSiswa.orElseThrow().setAlamat(siswa.getAlamat());
        currentSiswa.orElseThrow().setKelas(siswa.getKelas());

        siswaDetailsService.update(currentSiswa.get().getId());
        return new ResponseEntity<>(currentSiswa, HttpStatus.OK);

    }

    // ------------------- Delete Guru-----------------------------------------

    @RequestMapping(value = "/siswa/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deletesiswa(@PathVariable("id") long id) throws SQLException, ClassNotFoundException {
        logger.info("Fetching & Deleting Siswa with id {}", id);

        siswaDetailsService.delete(id);
        return new ResponseEntity<SiswaModel>(HttpStatus.NO_CONTENT);
    }
}

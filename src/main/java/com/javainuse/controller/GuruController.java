package com.javainuse.controller;

import com.javainuse.DTO.GuruDTO;
import com.javainuse.model.GuruModel;
import com.javainuse.service.JwtGuruDetailsService;
import com.javainuse.util.CustomErrorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:3000")
public class GuruController {

    public static final Logger logger = LoggerFactory.getLogger(DaftarPasienController.class);

    @Autowired
    private JwtGuruDetailsService guruDetailsService;


    //--------------------- Create a Game ---------------------------------

    @RequestMapping(value = "/guru/add", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> createGuru(@RequestBody GuruDTO guru) throws SQLException, ClassNotFoundException {
        logger.info("Creating Guru : {}",guru);

        guruDetailsService.save(guru);

        return new ResponseEntity<>(guru, HttpStatus.CREATED);
    }

    // -------------------Retrieve All Guru--------------------------------------------

    @RequestMapping(value = "/guru", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<GuruModel>> listAllGuru() throws SQLException, ClassNotFoundException {

        List<GuruModel> gurus = guruDetailsService.findAll();

        return new ResponseEntity<>(gurus, HttpStatus.OK);
    }

    // -------------------Retrieve Single Guru By Id------------------------------------------

    @RequestMapping(value = "/guru/id/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getGuru(@PathVariable("id") long id) throws SQLException, ClassNotFoundException {
        logger.info("Fetching Guru with id {}", id);

        Optional<GuruModel> guru = guruDetailsService.findById(id);

        if (guru == null) {
            logger.error("Game with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Guru with id " + id + " not found"), HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(guru, HttpStatus.OK);
    }

    // ------------------- Update Guru ------------------------------------------------
    @RequestMapping(value = "/guru/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateGuru(@PathVariable("id") long id, @RequestBody GuruDTO guru) throws SQLException, ClassNotFoundException {
        logger.info("Updating Movie with id {}", id);

        Optional<GuruModel> currentGuru = guruDetailsService.findById(id);

        if (currentGuru == null) {
            logger.error("Unable to update. Movie with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Unable to update. Guru with id " + id + " not found."), HttpStatus.NOT_FOUND);
        }
        currentGuru.orElseThrow().setGuru(guru.getGuru());
        currentGuru.orElseThrow().setLahir(guru.getLahir());
        currentGuru.orElseThrow().setAlamat(guru.getAlamat());


        guruDetailsService.update(currentGuru.get().getId());
        return new ResponseEntity<>(currentGuru, HttpStatus.OK);

    }

    // ------------------- Delete Guru-----------------------------------------

    @RequestMapping(value = "/guru/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteguru(@PathVariable("id") long id) throws SQLException, ClassNotFoundException {
        logger.info("Fetching & Deleting Game with id {}", id);

        guruDetailsService.delete(id);
        return new ResponseEntity<GuruModel>(HttpStatus.NO_CONTENT);
    }
}

package com.javainuse.controller;

import com.javainuse.DTO.KaryawanDTO;
import com.javainuse.model.KaryawanModel;
import com.javainuse.service.JwtKaryawanDetailsService;
import com.javainuse.util.CustomErrorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:3000")
public class KaryawanController {

    public static final Logger logger = LoggerFactory.getLogger(DaftarPasienController.class);

    @Autowired
    private JwtKaryawanDetailsService karyawanDetailsService;


    //--------------------- Create a Karyawan ---------------------------------

    @RequestMapping(value = "/karyawan/add", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> createKaryawan(@RequestBody KaryawanDTO karyawan) throws SQLException, ClassNotFoundException {
        logger.info("Creating Karyawan : {}",karyawan);

        karyawanDetailsService.save(karyawan);

        return new ResponseEntity<>(karyawan, HttpStatus.CREATED);
    }

    // -------------------Retrieve All Guru--------------------------------------------

    @RequestMapping(value = "/karyawan", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<KaryawanModel>> listAllKaryawan() throws SQLException, ClassNotFoundException {

        List<KaryawanModel> karyawans = karyawanDetailsService.findAll();

        return new ResponseEntity<>(karyawans, HttpStatus.OK);
    }

    // -------------------Retrieve Single Karyawan By Id------------------------------------------

    @RequestMapping(value = "/karyawan/id/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getKaryawan(@PathVariable("id") long id) throws SQLException, ClassNotFoundException {
        logger.info("Fetching Karyawan with id {}", id);

        Optional<KaryawanModel> karyawan = karyawanDetailsService.findById(id);

        if (karyawan == null) {
            logger.error("Karyawan with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Karyawan with id " + id + " not found"), HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(karyawan, HttpStatus.OK);
    }

    // ------------------- Update Karyawan ------------------------------------------------
    @RequestMapping(value = "/karyawan/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateKaryawan(@PathVariable("id") long id, @RequestBody KaryawanDTO karyawan) throws SQLException, ClassNotFoundException {
        logger.info("Updating Karyawan with id {}", id);

        Optional<KaryawanModel> currentKaryawan = karyawanDetailsService.findById(id);

        if (currentKaryawan == null) {
            logger.error("Unable to update. Karyawan with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Unable to update. Karyawan with id " + id + " not found."), HttpStatus.NOT_FOUND);
        }
        currentKaryawan.orElseThrow().setKaryawan(karyawan.getKaryawan());
        currentKaryawan.orElseThrow().setLahir(karyawan.getLahir());
        currentKaryawan.orElseThrow().setAlamat(karyawan.getAlamat());

        karyawanDetailsService.update(currentKaryawan.get().getId());
        return new ResponseEntity<>(currentKaryawan, HttpStatus.OK);

    }

    // ------------------- Delete Karyawan-----------------------------------------

    @RequestMapping(value = "/karyawan/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deletekaryawan(@PathVariable("id") long id) throws SQLException, ClassNotFoundException {
        logger.info("Fetching & Deleting Karyawan with id {}", id);

        karyawanDetailsService.delete(id);
        return new ResponseEntity<KaryawanModel>(HttpStatus.NO_CONTENT);
    }
}

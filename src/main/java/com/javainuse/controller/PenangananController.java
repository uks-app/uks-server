package com.javainuse.controller;

import com.javainuse.DTO.PenangananDTO;
import com.javainuse.model.PenangananModel;
import com.javainuse.service.JwtPenangananDetailsService;
import com.javainuse.util.CustomErrorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:3000")
public class PenangananController {

    public static final Logger logger = LoggerFactory.getLogger(PenangananController.class);

    @Autowired
    private JwtPenangananDetailsService penangananDetailsService;


    //--------------------- Create a Game ---------------------------------

    @RequestMapping(value = "/penanganan/add", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> createPenanganan(@RequestBody PenangananDTO penanganan) throws SQLException, ClassNotFoundException {
        logger.info("Creating Penanganan : {}",penanganan);

        penangananDetailsService.save(penanganan);

        return new ResponseEntity<>(penanganan, HttpStatus.CREATED);
    }

    // -------------------Retrieve All Guru--------------------------------------------

    @RequestMapping(value = "/penanganan", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<PenangananModel>> listAllPenanganan() throws SQLException, ClassNotFoundException {

        List<PenangananModel> penanganans = penangananDetailsService.findAll();

        return new ResponseEntity<>(penanganans, HttpStatus.OK);
    }

    // -------------------Retrieve Single Guru By Id------------------------------------------

    @RequestMapping(value = "/penanganan/id/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getPenanganan(@PathVariable("id") long id) throws SQLException, ClassNotFoundException {
        logger.info("Fetching penanganan with id {}", id);

        Optional<PenangananModel> penanganan = penangananDetailsService.findById(id);

        if (penanganan == null) {
            logger.error("penanganan with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("penanganan with id " + id + " not found"), HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(penanganan, HttpStatus.OK);
    }

    // ------------------- Update Guru ------------------------------------------------
    @RequestMapping(value = "/penanganan/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updatePenanganan(@PathVariable("id") long id, @RequestBody PenangananDTO penanganan) throws SQLException, ClassNotFoundException {
        logger.info("Updating penanganan with id {}", id);

        Optional<PenangananModel> currentPenanganan = penangananDetailsService.findById(id);

        if (currentPenanganan == null) {
            logger.error("Unable to update. penanganan with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Unable to update. diagnosa with id " + id + " not found."), HttpStatus.NOT_FOUND);
        }
        currentPenanganan.orElseThrow().setPenanganan(penanganan.getPenanganan());

        penangananDetailsService.update(currentPenanganan.get().getId());
        return new ResponseEntity<>(currentPenanganan, HttpStatus.OK);

    }

    // ------------------- Delete Guru-----------------------------------------

    @RequestMapping(value = "/penanganan/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deletepenanganan(@PathVariable("id") long id) throws SQLException, ClassNotFoundException {
        logger.info("Fetching & Deleting Penanganan with id {}", id);

        penangananDetailsService.delete(id);
        return new ResponseEntity<PenangananModel>(HttpStatus.NO_CONTENT);
    }
}

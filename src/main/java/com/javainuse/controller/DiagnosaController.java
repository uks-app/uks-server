package com.javainuse.controller;

import com.javainuse.DTO.DiagnosaDTO;
import com.javainuse.model.DiagnosaModel;
import com.javainuse.service.JwtDiagnosaDetailsService;
import com.javainuse.util.CustomErrorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:3000")
public class DiagnosaController {

    public static final Logger logger = LoggerFactory.getLogger(DiagnosaController.class);

    @Autowired
    private JwtDiagnosaDetailsService diagnosaDetailsService;


    //--------------------- Create a Game ---------------------------------

    @RequestMapping(value = "/diagnosa/add", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> createDiagnosa(@RequestBody DiagnosaDTO diagnosa) throws SQLException, ClassNotFoundException {
        logger.info("Creating Diagnosa : {}",diagnosa);

        diagnosaDetailsService.save(diagnosa);

        return new ResponseEntity<>(diagnosa, HttpStatus.CREATED);
    }

    // -------------------Retrieve All Guru--------------------------------------------

    @RequestMapping(value = "/diagnosa", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<DiagnosaModel>> listAllDiagnosa() throws SQLException, ClassNotFoundException {

        List<DiagnosaModel> diagnosas = diagnosaDetailsService.findAll();

        return new ResponseEntity<>(diagnosas, HttpStatus.OK);
    }

    // -------------------Retrieve Single Guru By Id------------------------------------------

    @RequestMapping(value = "/diagnosa/id/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getDiagnosa(@PathVariable("id") long id) throws SQLException, ClassNotFoundException {
        logger.info("Fetching diagnosa with id {}", id);

        Optional<DiagnosaModel> diagnosa = diagnosaDetailsService.findById(id);

        if (diagnosa == null) {
            logger.error("Diagnosa with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Diagnosa with id " + id + " not found"), HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(diagnosa, HttpStatus.OK);
    }

    // ------------------- Update Guru ------------------------------------------------
    @RequestMapping(value = "/diagnosa/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateDiagnosa(@PathVariable("id") long id, @RequestBody DiagnosaDTO diagnosa) throws SQLException, ClassNotFoundException {
        logger.info("Updating Diagnosa with id {}", id);

        Optional<DiagnosaModel> currentDiagnosa = diagnosaDetailsService.findById(id);

        if (currentDiagnosa == null) {
            logger.error("Unable to update. diagnosa with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Unable to update. diagnosa with id " + id + " not found."), HttpStatus.NOT_FOUND);
        }
        currentDiagnosa.orElseThrow().setDiagnosa(diagnosa.getDiagnosa());

        diagnosaDetailsService.update(currentDiagnosa.get().getId());
        return new ResponseEntity<>(currentDiagnosa, HttpStatus.OK);

    }

    // ------------------- Delete Guru-----------------------------------------

    @RequestMapping(value = "/diagnosa/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deletediagnosa(@PathVariable("id") long id) throws SQLException, ClassNotFoundException {
        logger.info("Fetching & Deleting Diagnosa with id {}", id);

        diagnosaDetailsService.delete(id);
        return new ResponseEntity<DiagnosaModel>(HttpStatus.NO_CONTENT);
    }
}

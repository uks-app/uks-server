package com.javainuse.controller;

import com.javainuse.DTO.SiswaDTO;
import com.javainuse.DTO.StatusDTO;

import com.javainuse.model.StatusModel;

import com.javainuse.service.JwtStatusDetailsService;
import com.javainuse.util.CustomErrorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:3000")
public class StatusController {

    public static final Logger logger = LoggerFactory.getLogger(DaftarPasienController.class);

    @Autowired
    private JwtStatusDetailsService statusDetailsService;


    //--------------------- Create a Status ---------------------------------

    @RequestMapping(value = "/status/add", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> createStatus(@RequestBody StatusDTO status) throws SQLException, ClassNotFoundException {
        logger.info("Creating Status : {}",status);

        statusDetailsService.save(status);

        return new ResponseEntity<>(status, HttpStatus.CREATED);
    }

    // -------------------Retrieve All Guru--------------------------------------------

    @RequestMapping(value = "/status", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<StatusModel>> listAllStatus() throws SQLException, ClassNotFoundException {

        List<StatusModel> statuss = statusDetailsService.findAll();

        return new ResponseEntity<>(statuss, HttpStatus.OK);
    }

    // -------------------Retrieve Single Guru By Id------------------------------------------

    @RequestMapping(value = "/status/id/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getStatus(@PathVariable("id") long id) throws SQLException, ClassNotFoundException {
        logger.info("Fetching Status with id {}", id);

        Optional<StatusModel> status = statusDetailsService.findById(id);

        if (status == null) {
            logger.error("status with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("status with id " + id + " not found"), HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(status, HttpStatus.OK);
    }

    // ------------------- Update status ------------------------------------------------
    @RequestMapping(value = "/status/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateStatus(@PathVariable("id") long id, @RequestBody StatusDTO status) throws SQLException, ClassNotFoundException {
        logger.info("Updating status with id {}", id);

        Optional<StatusModel> currentStatus = statusDetailsService.findById(id);

        if (currentStatus == null) {
            logger.error("Unable to update. status with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Unable to update. status with id " + id + " not found."), HttpStatus.NOT_FOUND);
        }
        currentStatus.orElseThrow().setStatus(status.getStatus());


        statusDetailsService.update(currentStatus.get().getId());
        return new ResponseEntity<>(currentStatus, HttpStatus.OK);

    }

    // ------------------- Delete Guru-----------------------------------------

    @RequestMapping(value = "/status/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deletestatus(@PathVariable("id") long id) throws SQLException, ClassNotFoundException {
        logger.info("Fetching & Deleting Status with id {}", id);

        statusDetailsService.delete(id);
        return new ResponseEntity<StatusModel>(HttpStatus.NO_CONTENT);
    }
}

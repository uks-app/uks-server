package com.javainuse.controller;

import com.javainuse.DTO.SiswaDTO;
import com.javainuse.DTO.TindakanDTO;
import com.javainuse.model.SiswaModel;
import com.javainuse.model.TindakanModel;
import com.javainuse.service.JwtSiswaDetailsService;
import com.javainuse.service.JwtTindakanDetailsService;
import com.javainuse.util.CustomErrorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:3000")
public class TindakanController {

    public static final Logger logger = LoggerFactory.getLogger(TindakanController.class);

    @Autowired
    private JwtTindakanDetailsService tindakanDetailsService;


    //--------------------- Create a Tindakan ---------------------------------

    @RequestMapping(value = "/tindakan/add", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> createTindakan(@RequestBody TindakanDTO tindakan) throws SQLException, ClassNotFoundException {
        logger.info("Creating Tindakan : {}",tindakan);

       tindakanDetailsService.save(tindakan);

        return new ResponseEntity<>(tindakan, HttpStatus.CREATED);
    }

    // -------------------Retrieve All Guru--------------------------------------------

    @RequestMapping(value = "/tindakan", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<TindakanModel>> listAllTindakan() throws SQLException, ClassNotFoundException {

        List<TindakanModel> tindakans = tindakanDetailsService.findAll();

        return new ResponseEntity<>(tindakans, HttpStatus.OK);
    }

    // -------------------Retrieve Single Guru By Id------------------------------------------

    @RequestMapping(value = "/tindakan/id/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getTindakan(@PathVariable("id") long id) throws SQLException, ClassNotFoundException {
        logger.info("Fetching Tindakan with id {}", id);

        Optional<TindakanModel> tindakan = tindakanDetailsService.findById(id);

        if (tindakan == null) {
            logger.error("Tindakan with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Tindakan with id " + id + " not found"), HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(tindakan, HttpStatus.OK);
    }

    // ------------------- Update Tindakan ------------------------------------------------
    @RequestMapping(value = "/tindakan/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateTindakan(@PathVariable("id") long id, @RequestBody TindakanDTO tindakan) throws SQLException, ClassNotFoundException {
        logger.info("Updating tindakan with id {}", id);

        Optional<TindakanModel> currentTindakan = tindakanDetailsService.findById(id);

        if (currentTindakan == null) {
            logger.error("Unable to update. Tindakan with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Unable to update. Tindakan with id " + id + " not found."), HttpStatus.NOT_FOUND);
        }
        currentTindakan.orElseThrow().setTindakan(tindakan.getTindakan());


        tindakanDetailsService.update(currentTindakan.get().getId());
        return new ResponseEntity<>(currentTindakan, HttpStatus.OK);

    }

    // ------------------- Delete Guru-----------------------------------------

    @RequestMapping(value = "/tindakan/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deletetindakan(@PathVariable("id") long id) throws SQLException, ClassNotFoundException {
        logger.info("Fetching & Deleting Tindakan with id {}", id);

        tindakanDetailsService.delete(id);
        return new ResponseEntity<TindakanModel>(HttpStatus.NO_CONTENT);
    }
}

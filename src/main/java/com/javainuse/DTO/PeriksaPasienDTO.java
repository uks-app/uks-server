package com.javainuse.DTO;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.javainuse.model.GuruModel;
import com.javainuse.model.KaryawanModel;
import com.javainuse.model.SiswaModel;
import com.javainuse.model.StatusModel;

public class PeriksaPasienDTO {

    private long id;
    private String keterangan;
    private SiswaModel siswa;
    private GuruModel guru;
    private KaryawanModel karyawan;
    private StatusModel status;

    public PeriksaPasienDTO() {

    }

    public PeriksaPasienDTO(long id, String keterangan, SiswaModel siswa, GuruModel guru, KaryawanModel karyawan, StatusModel status) {
        this.id = id;
        this.keterangan = keterangan;
        this.siswa = siswa;
        this.guru = guru;
        this.karyawan = karyawan;
        this.status = status;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
    public String getKeterangan() {
        return keterangan;
    }
    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }
    public SiswaModel getSiswa() {
        return siswa;
    }
    public void setSiswa(SiswaModel siswa) {
        this.siswa = siswa;
    }
    public GuruModel getGuru() {
        return guru;
    }
    public void setGuru(GuruModel guru) {
        this.guru = guru;
    }
    public KaryawanModel getKaryawan() {
        return karyawan;
    }
    public void setKaryawan(KaryawanModel karyawan) {
        this.karyawan = karyawan;
    }
    public StatusModel getStatus() {
        return status;
    }
    public void setStatus(StatusModel status) {
        this.status = status;
    }
}

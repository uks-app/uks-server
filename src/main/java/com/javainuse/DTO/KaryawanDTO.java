package com.javainuse.DTO;

import com.javainuse.model.StatusModel;

import java.util.Date;

public class KaryawanDTO {

    private long id;

    private String karyawan;

    private Date lahir;

    private String alamat;





    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getKaryawan() {
        return karyawan;
    }

    public void setKaryawan(String karyawan) {
        this.karyawan = karyawan;
    }

    public Date getLahir() {
        return lahir;
    }

    public void setLahir(Date lahir) {
        this.lahir = lahir;
    }

    public String getAlamat() {
        return alamat;
    }
    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    }

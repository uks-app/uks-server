package com.javainuse.DTO;

import com.javainuse.model.StatusModel;

import java.util.Date;

public class GuruDTO {

    private long id;

    private String guru;


    private Date lahir;

    private String alamat;


    public Date getLahir() {
        return lahir;
    }

    public void setLahir(Date lahir) {
        this.lahir = lahir;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getGuru() {
        return guru;
    }

    public void setGuru(String guru) {
        this.guru = guru;
    }



    public String getAlamat() {
        return alamat;
    }
    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }



}

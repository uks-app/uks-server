package com.javainuse.DTO;

import java.util.Date;

public class ObatDTO {

    private long id;

    private String obat;

    private String stock;

    private Date expired;





    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getObat() {
        return obat;
    }

    public void setObat(String obat) {
        this.obat = obat;
    }


    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public Date getExpired() {
        return expired;
    }

    public void setExpired(Date expired) {
        this.expired = expired;
    }
}

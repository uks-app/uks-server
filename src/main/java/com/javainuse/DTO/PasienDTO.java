package com.javainuse.DTO;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.sql.Date;

public class PasienDTO {

    private long id;

    private String pasien;

    @CreationTimestamp
    private Date create_at;

    @UpdateTimestamp
    private Date update_at;






    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPasien() {
        return pasien;
    }

    public void setPasien(String pasien) {
        this.pasien = pasien;
    }

    public Date getCreate_at() {
        return create_at;
    }

    public void setCreate_at(Date create_at) {
        this.create_at = create_at;
    }

    public Date getUpdate_at() {
        return update_at;
    }

    public void setUpdate_at(Date update_at) {
        this.update_at = update_at;
    }
}

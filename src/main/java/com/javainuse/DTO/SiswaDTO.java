package com.javainuse.DTO;

import com.javainuse.model.StatusModel;

import java.util.Date;

public class SiswaDTO {

    private long id;

    private String siswa;

    private Date lahir;

    private String alamat;

    private String kelas;





    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSiswa() {
        return siswa;
    }

    public void setSiswa(String siswa) {
        this.siswa = siswa;
    }

    public String getKelas() {
        return kelas;
    }

    public void setKelas(String kelas) {
        this.kelas = kelas;
    }

    public Date getLahir() {
        return lahir;
    }

    public void setLahir(Date lahir) {
        this.lahir = lahir;
    }

    public String getAlamat() {
        return alamat;
    }
    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }


}

package com.javainuse.service;

import com.javainuse.DTO.SiswaDTO;
import com.javainuse.model.SiswaModel;
import com.javainuse.repository.SiswaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class JwtSiswaDetailsService {

    @Autowired
    private SiswaRepository siswaDao;
    private long id;

    public JwtSiswaDetailsService() {
    }

    public SiswaModel save(SiswaDTO siswa) {

        SiswaModel newSiswa = new SiswaModel();
        newSiswa.setSiswa(siswa.getSiswa());
        newSiswa.setLahir(siswa.getLahir());
        newSiswa.setAlamat(siswa.getAlamat());
        newSiswa.setKelas(siswa.getKelas());



        return siswaDao.save(newSiswa);
    }


    public Optional<SiswaModel> findById(Long id) {
        return Optional.ofNullable(siswaDao.findById(id));
    }


    public List<SiswaModel> findAll() {
        List<SiswaModel> siswas = new ArrayList<>();
        siswaDao.findAll().forEach(siswas::add);
        return siswas;
    }


    public void delete(Long id) {
        SiswaModel siswa = siswaDao.findById(id);
        siswaDao.delete(siswa);
    }


    public SiswaModel update(Long id) {
        SiswaModel siswa = siswaDao.findById(id);
        return siswaDao.save(siswa);
    }

}
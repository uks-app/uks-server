package com.javainuse.service;

import com.javainuse.DTO.GuruDTO;
import com.javainuse.DTO.StatusDTO;
import com.javainuse.DTO.TindakanDTO;
import com.javainuse.model.GuruModel;
import com.javainuse.model.StatusModel;
import com.javainuse.model.TindakanModel;
import com.javainuse.repository.GuruRepository;
import com.javainuse.repository.StatusRepository;
import com.javainuse.repository.TindakanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class JwtStatusDetailsService {

    @Autowired
    private StatusRepository statusDao;
    private long id;

    public JwtStatusDetailsService() {
    }

    public StatusModel save(StatusDTO status) {

        StatusModel newStatus = new StatusModel();
        newStatus.setStatus(status.getStatus());


        return statusDao.save(newStatus);
    }


    public Optional<StatusModel> findById(Long id) {
        return Optional.ofNullable(statusDao.findById(id));
    }


    public List<StatusModel> findAll() {
        List<StatusModel> statuss = new ArrayList<>();
        statusDao.findAll().forEach(statuss::add);
        return statuss;
    }


    public void delete(Long id) {
        StatusModel status = statusDao.findById(id);
        statusDao.delete(status);
    }


    public StatusModel update(Long id) {
        StatusModel status = statusDao.findById(id);
        return statusDao.save(status);
    }

}
package com.javainuse.service;

import com.javainuse.DTO.GuruDTO;
import com.javainuse.model.GuruModel;
import com.javainuse.repository.GuruRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class JwtGuruDetailsService {

    @Autowired
    private GuruRepository guruDao;
    private long id;

    public JwtGuruDetailsService() {
    }

    public GuruModel save(GuruDTO guru) {

        GuruModel newGuru = new GuruModel();
        newGuru.setGuru(guru.getGuru());
        newGuru.setLahir(guru.getLahir());
        newGuru.setAlamat(guru.getAlamat());


        return guruDao.save(newGuru);
    }


    public Optional<GuruModel> findById(Long id) {
        return Optional.ofNullable(guruDao.findById(id));
    }


    public List<GuruModel> findAll() {
        List<GuruModel> gurus = new ArrayList<>();
        guruDao.findAll().forEach(gurus::add);
        return gurus;
    }


    public void delete(Long id) {
        GuruModel guru = guruDao.findById(id);
        guruDao.delete(guru);
    }


    public GuruModel update(Long id) {
        GuruModel guru = guruDao.findById(id);
        return guruDao.save(guru);
    }

}
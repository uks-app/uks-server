package com.javainuse.service;

import com.javainuse.DTO.ObatDTO;
import com.javainuse.model.ObatModel;
import com.javainuse.repository.ObatRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class JwtObatDetailsService {

    @Autowired
    private ObatRepository obatDao;
    private long id;

    public JwtObatDetailsService() {
    }

    public ObatModel save(ObatDTO obat) {

        ObatModel newObat = new ObatModel();
        newObat.setObat(obat.getObat());
        newObat.setStock(obat.getStock());
        newObat.setExpired(obat.getExpired());




        return obatDao.save(newObat);
    }


    public Optional<ObatModel> findById(Long id) {
        return Optional.ofNullable(obatDao.findById(id));
    }


    public List<ObatModel> findAll() {
        List<ObatModel> obats = new ArrayList<>();
        obatDao.findAll().forEach(obats::add);
        return obats;
    }


    public void delete(Long id) {
        ObatModel obat = obatDao.findById(id);
        obatDao.delete(obat);
    }


    public ObatModel update(Long id) {
        ObatModel obat = obatDao.findById(id);
        return obatDao.save(obat);
    }

}
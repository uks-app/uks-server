package com.javainuse.service;

import com.javainuse.DTO.KaryawanDTO;
import com.javainuse.model.KaryawanModel;
import com.javainuse.repository.KaryawanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class JwtKaryawanDetailsService {

    @Autowired
    private KaryawanRepository karyawanDao;
    private long id;

    public JwtKaryawanDetailsService() {
    }

    public KaryawanModel save(KaryawanDTO karyawan) {

        KaryawanModel newKaryawan = new KaryawanModel();
        newKaryawan.setKaryawan(karyawan.getKaryawan());
        newKaryawan.setLahir(karyawan.getLahir());
        newKaryawan.setAlamat(karyawan.getAlamat());



        return karyawanDao.save(newKaryawan);
    }


    public Optional<KaryawanModel> findById(Long id) {
        return Optional.ofNullable(karyawanDao.findById(id));
    }


    public List<KaryawanModel> findAll() {
        List<KaryawanModel> karyawans = new ArrayList<>();
        karyawanDao.findAll().forEach(karyawans::add);
        return karyawans;
    }


    public void delete(Long id) {
        KaryawanModel karyawan = karyawanDao.findById(id);
        karyawanDao.delete(karyawan);
    }


    public KaryawanModel update(Long id) {
        KaryawanModel karyawan = karyawanDao.findById(id);
        return karyawanDao.save(karyawan);
    }

}
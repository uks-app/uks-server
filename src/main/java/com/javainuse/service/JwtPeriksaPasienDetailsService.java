package com.javainuse.service;

import com.javainuse.DTO.PeriksaPasienDTO;
import com.javainuse.model.PeriksaPasienModel;
import com.javainuse.repository.ObatRepository;
import com.javainuse.repository.PeriksaPasienRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class JwtPeriksaPasienDetailsService {

    @Autowired
    private PeriksaPasienRepository PeriksaDao;
    private long id;

    public JwtPeriksaPasienDetailsService() {
    }

    public PeriksaPasienModel save(PeriksaPasienDTO periksa) {

        PeriksaPasienModel newPeriksa = new PeriksaPasienModel();

        newPeriksa.setStatus(periksa.getStatus());
        newPeriksa.setKeterangan(periksa.getKeterangan());
        newPeriksa.setSiswa(periksa.getSiswa());
        newPeriksa.setGuru(periksa.getGuru());
        newPeriksa.setKaryawan(periksa.getKaryawan());
        return PeriksaDao.save(newPeriksa);
    }


    public Optional<PeriksaPasienModel> findById(Long id) {
        return Optional.ofNullable(PeriksaDao.findById(id));
    }


    public List<PeriksaPasienModel> findAll() {
        List<PeriksaPasienModel> periksas = new ArrayList<>();
        PeriksaDao.findAll().forEach(periksas::add);
        return periksas;
    }


    public void delete(Long id) {
        PeriksaPasienModel periksa = PeriksaDao.findById(id);
        PeriksaDao.delete(periksa);
    }


    public PeriksaPasienModel update(Long id) {
        PeriksaPasienModel periksa = PeriksaDao.findById(id);
        return PeriksaDao.save(periksa);
    }

}
package com.javainuse.service;

import com.javainuse.DTO.PenangananDTO;
import com.javainuse.model.PenangananModel;
import com.javainuse.repository.PenangananRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class JwtPenangananDetailsService {

    @Autowired
    private PenangananRepository penangananDao;
    private long id;

    public JwtPenangananDetailsService() {
    }

    public PenangananModel save(PenangananDTO penanganan) {

        PenangananModel newPenanganan = new PenangananModel();
        newPenanganan.setPenanganan(penanganan.getPenanganan());



        return penangananDao.save(newPenanganan);
    }


    public Optional<PenangananModel> findById(Long id) {
        return Optional.ofNullable(penangananDao.findById(id));
    }


    public List<PenangananModel> findAll() {
        List<PenangananModel> penanganans = new ArrayList<>();
        penangananDao.findAll().forEach(penanganans::add);
        return penanganans;
    }


    public void delete(Long id) {
        PenangananModel penanganan = penangananDao.findById(id);
        penangananDao.delete(penanganan);
    }


    public PenangananModel update(Long id) {
        PenangananModel penanganan = penangananDao.findById(id);
        return penangananDao.save(penanganan);
    }

}
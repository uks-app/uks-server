package com.javainuse.service;

import com.javainuse.DTO.GuruDTO;
import com.javainuse.DTO.TindakanDTO;
import com.javainuse.model.GuruModel;
import com.javainuse.model.TindakanModel;
import com.javainuse.repository.GuruRepository;
import com.javainuse.repository.TindakanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class JwtTindakanDetailsService {

    @Autowired
    private TindakanRepository tindakanDao;
    private long id;

    public JwtTindakanDetailsService() {
    }

    public TindakanModel save(TindakanDTO tindakan) {

        TindakanModel newTindakan = new TindakanModel();
        newTindakan.setTindakan(tindakan.getTindakan());


        return tindakanDao.save(newTindakan);
    }


    public Optional<TindakanModel> findById(Long id) {
        return Optional.ofNullable(tindakanDao.findById(id));
    }


    public List<TindakanModel> findAll() {
        List<TindakanModel> tindakans = new ArrayList<>();
        tindakanDao.findAll().forEach(tindakans::add);
        return tindakans;
    }


    public void delete(Long id) {
        TindakanModel tindakan = tindakanDao.findById(id);
        tindakanDao.delete(tindakan);
    }


    public TindakanModel update(Long id) {
        TindakanModel tindakan = tindakanDao.findById(id);
        return tindakanDao.save(tindakan);
    }

}
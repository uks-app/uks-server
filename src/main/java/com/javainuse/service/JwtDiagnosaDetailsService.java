package com.javainuse.service;

import com.javainuse.DTO.DiagnosaDTO;
import com.javainuse.model.DiagnosaModel;
import com.javainuse.repository.DiagnosaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class JwtDiagnosaDetailsService {

    @Autowired
    private DiagnosaRepository diagnosaDao;
    private long id;

    public JwtDiagnosaDetailsService() {
    }

    public DiagnosaModel save(DiagnosaDTO diagnosa) {

        DiagnosaModel newDiagnosa = new DiagnosaModel();
        newDiagnosa.setDiagnosa(diagnosa.getDiagnosa());



        return diagnosaDao.save(newDiagnosa);
    }


    public Optional<DiagnosaModel> findById(Long id) {
        return Optional.ofNullable(diagnosaDao.findById(id));
    }


    public List<DiagnosaModel> findAll() {
        List<DiagnosaModel> diagnosas = new ArrayList<>();
        diagnosaDao.findAll().forEach(diagnosas::add);
        return diagnosas;
    }


    public void delete(Long id) {
        DiagnosaModel diagnosa = diagnosaDao.findById(id);
        diagnosaDao.delete(diagnosa);
    }


    public DiagnosaModel update(Long id) {
        DiagnosaModel diagnosa = diagnosaDao.findById(id);
        return diagnosaDao.save(diagnosa);
    }

}